#include "scene.h"

Scene::Scene() : _sceneState(SceneState::PLAY)
{}

Scene::~Scene()
{}

void Scene::initialize(int renderW, int renderH, int width, int height, int rows, int colomns)
{
    _sceneState = SceneState::PLAY;
    
    _tiles = std::make_pair(colomns, rows);
    _tileSceneSize = std::make_pair(renderW / _tiles.first, renderH / _tiles.second);
    _tileImageSize = std::make_pair(width / _tiles.first, height / _tiles.second);
    
    _grid.clear();
    _gridSort.clear();
    
    setGrid();
}   

void Scene::setGrid()
{
    for(int j = 0; j < _tiles.second; ++j)
    {
        for(int i = 0; i < _tiles.first; ++i)
        {
            std::pair<int,int> vec (i, j);
            _grid.push_back(vec);
            _gridSort.push_back(vec);
        }
    }
    std::random_shuffle(_grid.begin(), _grid.end());
}

void Scene::changePartOfImage(const std::vector<std::pair<int,int>>& clicks)
{
    std::pair<int,int> _clickFirst = clicks[0];
    std::pair<int,int> _clickSecond = clicks[1];

    if(_clickFirst == _clickSecond)
    {
        printf("%s\n", "click the same");
        return;
    }
    
    int first_tile = _clickFirst.second / _tileSceneSize.second * _tiles.second + _clickFirst.first / _tileSceneSize.first;
    int second_tile = _clickSecond.second / _tileSceneSize.second * _tiles.second + _clickSecond.first / _tileSceneSize.first;
    std::swap(_grid[first_tile], _grid[second_tile]);
    
    if(_grid == _gridSort)
    {
        _sceneState = SceneState::COMPLETE;
    }
}

void Scene::getImageRect(int i, int j, SDL_Rect* imgRect) const
{
    auto coordinate = _grid[j * _tiles.first + i];
    imgRect->x = coordinate.first * _tileImageSize.first;
    imgRect->y = coordinate.second * _tileImageSize.second;
    imgRect->w = _tileImageSize.first;
    imgRect->h = _tileImageSize.second;
}

const SceneState& Scene::getSceneState() const
{
    return _sceneState;
}

const std::pair<int, int>& Scene::getTiles() const
{
    return _tiles;
}

const std::pair<int, int>& Scene::getTileSceneSize() const
{
    return _tileSceneSize;
}
