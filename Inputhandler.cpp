#include "inputhandler.h"

InputHandler::InputHandler():_inputState(InputState::EMPTY)
{
}

void InputHandler::update()
{
    SDL_Event event;
    const Uint8* keystate = SDL_GetKeyboardState(0);
    
    _inputState = InputState::EMPTY;
    
    while(SDL_PollEvent(&event))
    {
        if(event.type == SDL_MOUSEBUTTONDOWN)
        {
            if(event.button.button == SDL_BUTTON_LEFT)
            {
                _inputState = InputState::LEFT_BUTTON;
                _mousePressPosition = std::make_pair(event.button.x, event.button.y);
            }
        }
        else if(keystate[SDL_SCANCODE_Q] == 1 || keystate[SDL_SCANCODE_ESCAPE] == 1)
        {
            _inputState = InputState::EXIT;
        }
    }
}

const InputState& InputHandler::getInputState() const
{
    return _inputState;
}

const std::pair<int,int>& InputHandler::getMousePressPosition() const
{
    return _mousePressPosition;
}
