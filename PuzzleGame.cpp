#include "puzzleGame.h"

PuzzleGame::PuzzleGame() : _gameState(GameState::PLAY), _level(0)
{}

PuzzleGame::PuzzleGame(int windowWidth, int windowHeight, int tiles) : _render(windowWidth, windowHeight), _gameState(GameState::PLAY), _level(0)
{
    initialize(tiles);
}	

PuzzleGame::~PuzzleGame()
{}
	
void PuzzleGame::initialize(int tiles)
{
    if(!readConfig())
    {
        _gameState = GameState::FAILURE;
        return;
    }
    
    _render.initialize(_data[_level]);
    _scene.initialize(_render.getWindowWidth(), _render.getWindowHeight(),
                    _render.getImageWidth(), _render.getImageHeight(),
                    tiles, tiles);
    _render.render(_scene);
    _gameState = GameState::PLAY;
}

void PuzzleGame::setClick(const std::pair<int, int>& coordinate)
{
    _clicks.push_back(coordinate);
    if(_clicks.size() == 2)
    {
        _scene.changePartOfImage(_clicks);
        _gameState = GameState::UPDATE_CELLS;
        _clicks.clear();
    }
}

void PuzzleGame::loadNewGame()
{
    _level++;
    if (_level > _data.size() - 1)
    {
        _level = 0;
    }
    
    int tile = rand() % 4 + 2;
    
    _render.loadNewGame(_data[_level]);
    
    _scene.initialize(_render.getWindowWidth(), _render.getWindowHeight(),
                             _render.getImageWidth(), _render.getImageHeight(),
                             tile, tile);
    _render.render(_scene);
    _gameState = GameState::PLAY;
}

void PuzzleGame::render()
{
    _inputH.update();
    
    if(_scene.getSceneState() == SceneState::COMPLETE)
    {
        _gameState = GameState::COMPLETE;
        loadNewGame();
    }
    
    if(_inputH.getInputState() == InputState::LEFT_BUTTON)
    {
        if(_gameState != GameState::COMPLETE)
        {
            auto coordinate = _inputH.getMousePressPosition();
            setClick(coordinate);
        }
    }
    else if(_inputH.getInputState() == InputState::EXIT)
    {
        _gameState = GameState::EXIT;
    }
    
    if(_gameState == GameState::UPDATE_CELLS)
    {
        _render.render(_scene);
        _gameState = GameState::PLAY;
    }
}

bool PuzzleGame::readConfig()
{
	std::string line;
  	std::ifstream configfile ("data.ini");
  	if (configfile.is_open())
  	{
    	while ( configfile.good() )
    	{
      		getline (configfile,line);
      		_data.push_back(line);
    	}
        configfile.close();
  	}
  	else
    {
        printf("%s\n", "Unable to open file");
        return false;
    }

  	if(_data.empty())
    {
  		printf("%s\n", "No data in file!");
        return false;
    }
    
    return true;
}

const GameState& PuzzleGame::getGameState() const
{
    return _gameState;
}
