#pragma once
 
#include <algorithm>   
#include <vector>

#include "windowRender.h"

enum class SceneState
{
    PLAY,
    COMPLETE
};

class Scene
{
public:
	explicit Scene();
	~Scene();

    Scene(const Scene&) = delete;
    Scene& operator=(const Scene&) = delete;
    
	void initialize(int renderW, int renderH, int width, int height, int rows, int colomns);
    void changePartOfImage(const std::vector<std::pair<int,int>>& clicks);
    
    void getImageRect(int i, int j, SDL_Rect* imgRect) const;
    const SceneState& getSceneState() const;
    const std::pair<int, int>& getTiles() const;
    const std::pair<int, int>& getTileSceneSize() const;
    
private:
    SceneState _sceneState;

    std::pair<int, int> _tiles;
    std::pair<int, int> _tileSceneSize;
    std::pair<int, int> _tileImageSize;
    
    std::vector<std::pair<int, int>> _grid;
    std::vector<std::pair<int, int>> _gridSort;
    
    void setGrid();
};
