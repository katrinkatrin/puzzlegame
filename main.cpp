#include "puzzleGame.h"

int main(int argc, char* argv[])
{
    int windowWidth = 640;
    int windowHeight = 480;
    
    int tiles = 3;
    
	PuzzleGame puzzle(windowWidth, windowHeight, tiles);
    
    if (puzzle.getGameState() == GameState::FAILURE)
    {
        return 0;
    }

    while(puzzle.getGameState() != GameState::EXIT)
    {
		puzzle.render();
    }

	return 0;
}
