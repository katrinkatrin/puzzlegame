#include "render.h"

Render::Render() : WindowRender(0, 0), _window(nullptr), _renderer(nullptr), _sceneSurface(nullptr), _sceneTex(nullptr)
{
    _screenRect = new SDL_Rect();
    _imgRect = new SDL_Rect();
}

Render::Render(int windowWidth, int windowHeight) : WindowRender(windowWidth, windowHeight),
_window(nullptr), _renderer(nullptr), _sceneSurface(nullptr), _sceneTex(nullptr)
{
    _screenRect = new SDL_Rect();
    _imgRect = new SDL_Rect();
}

Render::~Render()
{
    delete _imgRect;
    delete _screenRect;
    SDL_DestroyTexture(_sceneTex);
    SDL_FreeSurface(_sceneSurface);
    SDL_DestroyRenderer(_renderer);
    SDL_DestroyWindow(_window);
    SDL_Quit();
}

void Render::initialize(const std::string& fileName)
{
    if(SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        printf("%s: %s\n", "SDL_Init Error ", SDL_GetError());
        SDL_Quit();
        exit(1);
    }
    
    _window = SDL_CreateWindow("Puzzle Game", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                               _windowWidth, _windowHeight, SDL_WINDOW_SHOWN);

    if(_window != nullptr)
    {
        _renderer = SDL_CreateRenderer(_window, -1, 0);
        initializeSceneSurface(fileName);
    }
}

void Render::initializeSceneSurface(const std::string& fileName)
{
    _sceneSurface = IMG_Load(fileName.c_str());
    
    if (_sceneSurface == nullptr) {
        fprintf (stderr, "Cannot load image: %s\n",  IMG_GetError());
        exit(EXIT_FAILURE);
    }
    
    _sceneTex = SDL_CreateTextureFromSurface(_renderer, _sceneSurface);
}

void Render::loadNewGame(const std::string& fileName)
{
    SDL_DestroyTexture(_sceneTex);
    SDL_FreeSurface(_sceneSurface);
    
    initializeSceneSurface(fileName);
}

void Render::render(const Scene& scene)
{
    SDL_RenderClear(_renderer);
    
    auto tiles = scene.getTiles();
    
    for(int j = 0; j < tiles.second; ++j)
    {
        for(int i = 0; i < tiles.first; ++i)
        {
            scene.getImageRect(i, j, _imgRect);
            
            auto partScreen = scene.getTileSceneSize();
            _screenRect->x = i * partScreen.first;
            _screenRect->y = j * partScreen.second;
            _screenRect->w = partScreen.first;
            _screenRect->h = partScreen.second;
            
            SDL_RenderCopy(_renderer, _sceneTex, _imgRect, _screenRect);
        }
    }
    SDL_RenderPresent(_renderer);
}

int Render::getImageWidth() const
{
    return _sceneSurface->w;
}

int Render::getImageHeight() const
{
    return _sceneSurface->h;
}
