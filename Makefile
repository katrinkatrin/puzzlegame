SRCFILES = puzzleGame.cpp inputhandler.cpp scene.cpp render.cpp main.cpp

HDRFILES = puzzleGame.h windowRender.h inputhandler.h scene.h render.h

LIBS=-L/usr/local/Cellar/sdl2/2.0.10/lib -L/usr/local/Cellar/sdl2_image/2.0.5/lib -L/usr/local/Cellar/sdl2_mixer/2.0.4/lib -L/usr/local/Cellar/sdl2_ttf/2.0.15/lib

LIBFILES = $(LIBS) -lSDL2  -lSDL2_image

CFLAGS = $(shell sdl2-config --cflags)

render:  $(SRCFILES) $(HDRFILES)
		g++ -g -DDEBUG $(CFLAGS) -std=c++11 -o render $(SRCFILES) $(LIBFILES)

clean: 
		rm render
