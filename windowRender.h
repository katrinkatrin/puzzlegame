#pragma once

#include <SDL2/SDL.h>

class WindowRender
{
public:
    explicit WindowRender():
    _windowWidth(0), _windowHeight(0)
    {}
    
    explicit WindowRender(int windowWidth, int windowHeight) :
    _windowWidth(windowWidth), _windowHeight(windowHeight)
    {}
    
    virtual ~WindowRender()
    {}
    
    int getWindowWidth() const {return _windowWidth;}
    int getWindowHeight() const {return _windowHeight;}
    
protected:
    int _windowWidth;
    int _windowHeight;
};
