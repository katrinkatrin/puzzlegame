#pragma once

#include <fstream>
#include <string>
#include <vector>

#include "inputhandler.h"
#include "render.h"
#include "scene.h"

enum class GameState
{
    PLAY,
    UPDATE_CELLS,
    COMPLETE,
    EXIT,
    FAILURE
};

class PuzzleGame
{
public:
    explicit PuzzleGame();
    explicit PuzzleGame(int windowWidth, int windowHeight, int tiles);
    ~PuzzleGame();
    
    PuzzleGame(const PuzzleGame&) = delete;
    PuzzleGame& operator=(const PuzzleGame&) = delete;
    
    void render();
    const GameState& getGameState() const;

private:	
    InputHandler _inputH;
    Render _render;
    Scene _scene;
    
    GameState _gameState;
    int _level;

	std::vector<std::string> _data;
    std::vector<std::pair<int, int>> _clicks;
    
    void initialize(int tiles);
	bool readConfig();
    void setClick(const std::pair<int, int>& coordinate);
    void loadNewGame();
};
