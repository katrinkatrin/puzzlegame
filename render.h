#pragma once

#include <string>

#include <SDL2/SDL_surface.h>
#include <SDL2/SDL_image.h>

#include "scene.h"
#include "windowRender.h"

class Render : public WindowRender
{
public:
    explicit Render();
    explicit Render(int windowWidth, int windowHeight);
    ~Render();
    
    Render(const Render&) = delete;
    Render& operator=(const Render&) = delete;
    
    void initialize(const std::string& fileName);
    void loadNewGame(const std::string& fileName);
    void render(const Scene& scene);
    
    int getImageWidth() const;
    int getImageHeight() const;
    
private:
    SDL_Window*     _window;
    SDL_Renderer*   _renderer;
    SDL_Surface*    _sceneSurface;
    SDL_Texture*    _sceneTex;
    
    SDL_Rect*       _screenRect;
    SDL_Rect*       _imgRect;
    
    void            initializeSceneSurface(const std::string& fileName);
};
