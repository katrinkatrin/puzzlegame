#pragma once 

#include <utility>
#include <SDL2/SDL.h>

enum class InputState
{
    EMPTY,
    LEFT_BUTTON,
    EXIT
};

class InputHandler
{
public:
    explicit InputHandler();
    ~InputHandler(){}
    
    InputHandler(const InputHandler&) = delete;
    InputHandler& operator=(const InputHandler&) = delete;

    void update();
    
    const InputState& getInputState() const;
    const std::pair<int,int>& getMousePressPosition() const;
    
private:
    InputState          _inputState;
    std::pair<int,int>  _mousePressPosition;
};
